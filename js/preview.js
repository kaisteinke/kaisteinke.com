function preview(src) {
    // create a image container that is almost full screen
    let container = document.createElement("div");
    container.classList.add("preview-container");
    container.style.position = "fixed";
    container.style.top = "0";
    container.style.left = "0";
    container.style.width = "100%";
    container.style.height = "100%";
    container.style.backgroundColor = "rgba(0,0,0,0.8)";
    container.style.zIndex = "1000";
    container.onclick = () => {
        // fade out the container
        container.style.opacity = "0";
        container.style.cursor = "default";

        // remove the container after the fade out animation is done
        setTimeout(() => {
            container.remove();
        }, 600);
    };

    // create a image element
    let img = document.createElement("img");
    img.src = src;
    img.style.maxWidth = "100%";
    img.style.maxHeight = "100%";
    img.style.position = "absolute";
    img.style.top = "50%";
    img.style.left = "50%";
    img.style.transform = "translate(-50%, -50%)";

    // add the image to the container
    container.appendChild(img);

    // add the container to the body
    document.body.appendChild(container);

    // fade in the container
    setTimeout(() => {
        container.style.opacity = "1";
    }, 100);
}