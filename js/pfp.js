document.getElementById("pfp").addEventListener("click", () => {
    // rotate the pfp
    document.getElementById("pfp").classList.add("rotate");

    // for every item that has the social-icon class, rotate it
    document.querySelectorAll(".social-icons").forEach((item) => {
        item.classList.add("rotate");
    });

    // flip body
    document.querySelector("body").classList.toggle("flipped");

    // after 0.6 seconds, remove the rotate class
    setTimeout(() => {
        document.getElementById("pfp").classList.remove("rotate");
        document.querySelectorAll(".social-icons").forEach((item) => {
            item.classList.remove("rotate");
        });
    }, 600);

    // create 60 divs filled with 8-ball emojis
    for (let i = 0; i < 60; i++) {
        let div = document.createElement("div");
        div.innerHTML = "🎱";
        div.style.position = "absolute";
        // random location
        div.style.top = `${Math.floor(Math.random() * 90)}%`;
        div.style.left = `${Math.floor(Math.random() * 90)}%`;
        // random size
        div.style.fontSize = `${Math.floor(Math.random() * 30) + 10}px`;
        div.style.transition = "all 0.5s ease";
        // make it rotate
        div.classList.add("icon-rotate");

        // append the div to the body
        document.body.appendChild(div);

        // fade in
        setTimeout(() => {
            div.style.opacity = "1";
        }, 50);

        // after a random amount of time, remove the div
        setTimeout(() => {
            // fade out
            div.style.opacity = "0";
            setTimeout(() => {
                div.remove();
            }, 600);
        }, Math.floor(Math.random() * 2000));
    }
});