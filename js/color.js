// Script to change color mode 
// (c) 2022 Kai Steinke <kai@steinke.dev>
// Licensed under the MIT License
const darkBG = "#111";
const darkText = "#FDFEFE";
const lightBG = "#fff";
const lightText = "#000";
const rootElem = document.querySelector(":root");
const modeicon = document.getElementById("mode-icon");

modeicon.addEventListener("click", toggleMode);

function toggleMode() {
    getComputedStyle(document.querySelector(':root')).getPropertyValue('--background-color') === lightBG ? changeModeTo('dark') : changeModeTo('light');
}

function changeModeTo(mode) {
    switch (mode) {
        case 'dark':
            rootElem.style.setProperty("--background-color", darkBG);
            rootElem.style.setProperty("--text-color", darkText);
            modeicon.classList.remove("flipped");
            break;
        case 'light':
            rootElem.style.setProperty("--background-color", lightBG);
            rootElem.style.setProperty("--text-color", lightText);
            modeicon.classList.add("flipped");
            break;
    }

    localStorage.setItem('mode', mode);
}

window.onload = () => {
    if (localStorage.getItem('mode') !== null) {
        const currentBGColor = getComputedStyle(document.querySelector(':root')).getPropertyValue('--background-color');
        if (localStorage.getItem('mode') === 'dark' && currentBGColor === lightBG) {
            changeModeTo('dark');
        } else if (localStorage.getItem('mode') === 'light' && currentBGColor === darkBG) {
            changeModeTo('light');
        }
    }
};